package com.stg.startthefirenoun

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class FirenounApplication : Application()