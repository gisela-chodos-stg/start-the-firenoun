package com.stg.startthefirenoun.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.stg.startthefirenoun.R
import com.stg.startthefirenoun.databinding.FragmentChorusBinding
import com.stg.startthefirenoun.presentation.models.Chorus
import com.stg.startthefirenoun.presentation.viewmodels.ChorusViewModel
import dagger.hilt.android.AndroidEntryPoint

const val KEY_CHORUS = "key_chorus"

@AndroidEntryPoint
class ChorusFragment : Fragment() {

    val args: ChorusFragmentArgs by navArgs()

    private lateinit var chorus: Chorus

    private val viewModel: ChorusViewModel by viewModels()

    // view binding
    private var _binding: FragmentChorusBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentChorusBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)


        chorus = args.chorus
        setupChorusLines()
    }

    private fun setupChorusLines() {
        binding.txvLineOne.text = resources.getString(R.string.chorus_line_one, chorus.blankOne, chorus.blankTwo)
        binding.txvLineTwo.text = resources.getString(R.string.chorus_line_two, chorus.blankThree, chorus.blankFour, chorus.blankFive)
        binding.txvLineThree.text = resources.getString(R.string.chorus_line_three, chorus.blankSix, chorus.blankSeven)
        binding.txvLineFour.text = resources.getString(R.string.chorus_line_four, chorus.blankEight, chorus.blankNine)
    }

}