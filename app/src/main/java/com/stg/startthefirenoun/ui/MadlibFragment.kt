package com.stg.startthefirenoun.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.findNavController
import com.stg.startthefirenoun.databinding.FragmentMadlibBinding
import com.stg.startthefirenoun.presentation.models.Chorus
import com.stg.startthefirenoun.presentation.viewmodels.MadlibViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MadlibFragment : Fragment() {

    private val viewModel: MadlibViewModel by viewModels()

    // view binding
    private var _binding: FragmentMadlibBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentMadlibBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(false)

        binding.btnSave.setOnClickListener {
            val chorus = Chorus(
                binding.etxBlank1.text.toString().trim(),
                binding.etxBlank2.text.toString().trim(),
                binding.etxBlank3.text.toString().trim(),
                binding.etxBlank4.text.toString().trim(),
                binding.etxBlank5.text.toString().trim(),
                binding.etxBlank6.text.toString().trim(),
                binding.etxBlank7.text.toString().trim(),
                binding.etxBlank8.text.toString().trim(),
                binding.etxBlank9.text.toString().trim(),
            )

            val action = MadlibFragmentDirections.actionMadlibToChorus(chorus)
            view.findNavController().navigate(action)
        }
    }

    override fun onDestroy() {
        _binding = null
        super.onDestroy()
    }

}