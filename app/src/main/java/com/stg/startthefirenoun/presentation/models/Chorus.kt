package com.stg.startthefirenoun.presentation.models

import java.io.Serializable

data class Chorus(
    val blankOne: String = "",
    val blankTwo: String = "",
    val blankThree: String = "",
    val blankFour: String = "",
    val blankFive: String = "",
    val blankSix: String = "",
    val blankSeven: String = "",
    val blankEight: String = "",
    val blankNine: String = "",
) : Serializable